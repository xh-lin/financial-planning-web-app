from application import app, db
from application.models import Authority, User, Post, Section, Thread, EditInfo
from datetime import datetime, timedelta
from sqlalchemy import desc, func
from sqlalchemy.sql import label


@app.shell_context_processor
def make_shell_context():
  """For debugging in shell.
  no longer needed to import by hand when debugging in shell
  remember to update here when models.py is updated
  Example:
  (venv) $ flask shell
  """
  return {'db': db, 'desc': desc, 'func': func, 'label': label, 'Authority': Authority,
      'datetime': datetime, 'timedelta': timedelta,
      'User': User, 'Post': Post, 'Section': Section, 'Thread': Thread, 'EditInfo': EditInfo}