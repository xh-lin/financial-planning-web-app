from flask_mail import Message
from application import mail
from flask import render_template
from application import app
from threading import Thread


def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)

#takes information from send user reset email method and sends the information to the users email
def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    Thread(target=send_async_email, args=(app, msg)).start()

#will send password reset link to users email
def send_password_reset_email(user):
    token = user.get_reset_password_token()
    send_email('Reset Your Password',
               sender='noreply@thoroughway.com',
               recipients=[user.email],
               text_body=render_template('user/reset_password.txt',
                                         user=user, token=token),
               html_body=render_template('user/reset_password.html',
                                         user=user, token=token))
