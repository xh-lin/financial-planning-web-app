from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_moment import Moment
from flask_mail import Mail
import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
import os

app = Flask(__name__)
app.config['RECAPTCHA_USE_SSL']= False
app.config['RECAPTCHA_PUBLIC_KEY']= '6Lecd_kZAAAAABPyX8UgxO9O2j-4GA0YTpU2vRgy'
app.config['RECAPTCHA_PRIVATE_KEY']='6Lecd_kZAAAAAIRID4KmCAJhKkpS3K5PjDJxM6Af'
app.config['RECAPTCHA_OPTIONS'] = {'theme':'white'}

app.config.update(
    MAIL_SERVER='smtp.googlemail.com',
    MAIL_PORT=587,
    MAIL_USE_TLS=1,
    MAIL_USERNAME = 'financialplanningproject128@gmail.com',
    MAIL_PASSWORD = 'rtcyutnqptdleyux'
)

mail = Mail(app)
# configuration items in Config can be accessed with a dictionary syntax from app.config
app.config.from_object(Config)

########## initialize extensions ##########

db = SQLAlchemy(app) # database object
# migration engine
# SQLite doesn't allow modify/delete columns
# enable render_as_batch solves it by copying to a new table when migrating
migrate = Migrate(app, db, render_as_batch=True) 
login = LoginManager(app)
login.login_view = 'login' # tell Flask-Login which view function handles logins
# for time display format
moment = Moment(app)

########## Debug ##########

# set the FLASK_ENV variable in .flaskenv to be development will enable debug mode
if not app.debug:
  # enable the email logger when the application isn't running debug mode
  # and when the email server exists in the configuration (.flaskenv)
  if app.config['MAIL_SERVER']:
    auth = None
    if app.config['MAIL_USERNAME'] or app.config['MAIL_PASSWORD']:
      auth = (app.config['MAIL_USERNAME'], app.config['MAIL_PASSWORD'])
    secure = None
    if app.config['MAIL_USE_TLS']:
      secure = ()
    mail_handler = SMTPHandler(
      mailhost=(app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
      fromaddr='no-reply@' + app.config['MAIL_SERVER'],
      toaddrs=app.config['ADMINS'], subject='Microblog Failure',
      credentials=auth, secure=secure)
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)

    # logging to a file
    if not os.path.exists('logs'):
      os.mkdir('logs')
    # RotatingFileHandler ensuring that the log files do not grow too large
    # limiting the size of the log file to 10KB, keeping the last 10 log files as backup.
    file_handler = RotatingFileHandler('logs/microblog.log', maxBytes=10240, backupCount=10)
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    # logging categories are DEBUG, INFO, WARNING, ERROR and CRITICAL in increasing order
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.INFO)
    app.logger.info('Microblog startup')


# import modules after the application instance is created
from application import routes, models, errors