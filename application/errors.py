# custom error pages
from flask import render_template
from application import app, db

@app.errorhandler(404)
def not_found_error(error):
  """Renders a custom error page for users and returns the error code
  """
  return render_template('error/404.html'), 404

@app.errorhandler(500)
def internal_error(error):
  """Renders a custom error page for users and returns the error code
  """
  # because 500 errors could be invoked after a database error
  # so reset the database session to a clean state
  db.session.rollback()
  return render_template('error/500.html'), 500