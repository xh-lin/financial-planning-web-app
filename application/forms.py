from flask_wtf import FlaskForm, RecaptchaField
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField, \
    IntegerField, validators, ValidationError
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Regexp, Length
from application.models import User
import re
"""
Forms.py is responsible for all of the forms on the website.
It is also responsible for creating validators to validate the information that the user types in the fields in the forms.
"""
# The criteria for a username
def unique_username(form, username):
  user = User.query.filter_by(username=username.data).first()
  if user is not None:
    raise ValidationError('Please use a different username.')

# Makes sure the display name is not already taken by another user.
def unique_displayname(form, displayname):
  user = User.query.filter_by(displayname=displayname.data).first()
  if user is not None:
    raise ValidationError('Please use a different display name.')

# makes sure email is unique
def unique_email(form, email):
  user = User.query.filter_by(email=email.data).first()
  if user is not None:
    raise ValidationError('Please use a different email address.')

# The criteria for a complex password.
# A password using this criteria is required!
def complex_password(form, password):
  contains_digit_regex = re.compile('.*?\d')
  contains_lower_regex = re.compile('.*?[a-z]')
  contains_upper_regex = re.compile('.*?[A-Z]')
  if not contains_digit_regex.match(password.data):
    raise ValidationError('At least 1 digit.')
  if not contains_lower_regex.match(password.data):
    raise ValidationError('At least 1 lower case letter.')
  if not contains_upper_regex.match(password.data):
    raise ValidationError('At least 1 upper case letter.')

""" note:
  any methods matchs the pattern validate_<field_name>,
  WTForms invokes them in addition to the stock validators
"""
# Form to Login to the site with validators
class LoginForm(FlaskForm):
  username = StringField('Username', validators=[DataRequired()])
  password = PasswordField('Password', validators=[DataRequired()])
  remember_me = BooleanField('Remember Me')
  submit = SubmitField('Sign In')

# Form to Register for the site with validators
class RegistrationForm(FlaskForm):
  # \w* == [0-9a-zA-Z_]*, \A match start, \Z match end
  username = StringField('Username', validators=[Length(min=3, max=30),
      Regexp('\A\w*\Z'), unique_username])
  # allow space but not at the start and the end
  displayname = StringField('Display Name', validators=[Length(min=3, max=30),
      Regexp('\A(?=\w)[ \w]*(?<=\w)\Z'), unique_displayname])
  email = StringField('Email', validators=[Length(min=3, max=150), Email(),
      unique_email])
  password = PasswordField('Password', validators=[Length(min=6, max=30),
      complex_password])
  password2 = PasswordField('Repeat Password', validators=[DataRequired(), EqualTo('password')])
  recaptcha = RecaptchaField()
  submit = SubmitField('Register')

# Form to edit a users profile
class EditProfileForm(FlaskForm):
  # allow space but not at the start and the end
  displayname = StringField('Display Name', validators=[Length(min=3, max=30),
      Regexp('\A(?=\w)[ \w]*(?<=\w)\Z')])
  # max variable must not greater than the size of about_me field in User model
  about_me = TextAreaField('About me', validators=[Length(min=0, max=200)])
  submit = SubmitField('Submit')

  def __init__(self, original_displayname, *args, **kwargs):
    super(EditProfileForm, self).__init__(*args, **kwargs)
    self.original_displayname = original_displayname

  def validate_displayname(self, displayname):
    # allow the field to be the initial displayname
    if displayname.data != self.original_displayname:
      unique_displayname(self, displayname)

# Form to edit the users email
class EditEmailForm(FlaskForm):
  email = StringField('New Email', validators=[Length(min=3, max=150), Email()])
  submit = SubmitField('Save')

  def __init__(self, original_email, *args, **kwargs):
    super(EditEmailForm, self).__init__(*args, **kwargs)
    self.original_email = original_email

  def validate_email(self, email):
    # don't allow the new email to be the same as the current one
    if email.data == self.original_email:
      raise ValidationError('This is the current email address.')
    unique_email(self, email)


class EditPasswordForm(FlaskForm):
  current_password = PasswordField('Current Password', validators=[DataRequired()])
  password = PasswordField('New Password', validators=[Length(min=6, max=30),
      complex_password])
  password2 = PasswordField('Repeat New Password', validators=[DataRequired(), EqualTo('password')])
  submit = SubmitField('Save')

  def __init__(self, current_user, *args, **kwargs):
    super(EditPasswordForm, self).__init__(*args, **kwargs)
    self.current_user = current_user

  def validate_current_password(self, current_password):
    if not self.current_user.check_password(current_password.data):
      raise ValidationError('Incorrect password.')


# for posting a thread
class CreateThreadForm(FlaskForm):
  topic = StringField('Topic', validators=[Length(min=3, max=60)])
  body = TextAreaField('Context', validators=[Length(min=3, max=300)])
  submit = SubmitField('Post')

# for editing a thread (i.e. the topic and the topic post)
class EditThreadForm(FlaskForm):
  topic = StringField('Topic', validators=[Length(min=3, max=60)])
  body = TextAreaField('Context', validators=[Length(min=3, max=300)])
  reason = TextAreaField('Reason for modification', validators=[Length(min=0, max=150)])
  submit = SubmitField('Save')

# for replying to a thread
class ReplyThreadForm(FlaskForm):
  body = TextAreaField('Context', validators=[Length(min=3, max=300)])
  submit_reply = SubmitField('Reply')


# for editing/changing a post
class EditPostForm(FlaskForm):
  body = TextAreaField('Context', validators=[Length(min=3, max=300)])
  reason = TextAreaField('Reason for modification', validators=[Length(min=0, max=150)])
  submit_save = SubmitField('Save')


# for admin to create a new forum section
class SectionForm(FlaskForm):
  name = StringField('Section Name', validators=[Length(min=3, max=30)])
  slogan = TextAreaField('Slogen (short description)', validators=[Length(min=0, max=60)])
  description = TextAreaField('Full Description', validators=[Length(min=0, max=200)])
  submit = SubmitField('Create')

# Reset password request form.
class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')

#Password reset form template.
class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Request Password Reset')

class ContactForm(FlaskForm):
  name = StringField("Your Name",  [validators.Required()])
  email = StringField("Your E-mail",  [validators.Required()])
  subject = StringField("Subject",  [validators.Required()])
  message = TextAreaField("Message",  [validators.Required()])
  submit = SubmitField("Send")
