"""Tables stored in the database.
  need to generate a database migration every time a table is modified:
  (venv) $ flask db migrate -m "new fields in x model"
  (venv) $ flask db upgrade
"""
from application import db, login, app
from datetime import datetime
from time import time
import jwt
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from hashlib import md5
from sqlalchemy import desc
from enum import Enum

# level of user authorities
# admin and webmaster are considered as superusers
# webmaster can give other users admin authority
Authority = Enum('Authority', 'banned user admin webmaster')

# many-to-many relationship between User and Thread
viewers = db.Table('viewers',
  db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
  db.Column('thread_id', db.Integer, db.ForeignKey('thread.id'), primary_key=True)
)

# allow Flask-Login to load users
@login.user_loader
def load_user(id):
  return User.query.get(int(id))


class User(UserMixin, db.Model):
  """       User
  id              INTEGER
  username        VARCHAR (30)
  displayname     VARCHAR (30)
  email           VARCHAR (150)
  password_hash   VARCHAR (150)
  about_me        VARCHAR (200)
  last_seen       DATETIME
  authority       VARCHAR (20)
  posts           -> [Post,...]       one-to-many relationship (one-side)
  viewed_threads  -> [Thread, ...]    many-to-many relationship
  edit_history    -> [EditInfo, ...]  one-to-many relationship (one-side)

  Example:
  >>> u = User(username='test', displayname='Test Account', email='test@example.com', 
          about_me='my pw is not test')
  >>> u.set_password('test')
  >>> u.check_password('test')
  >>> db.session.add(u)
  >>> db.session.commit()
  >>> User.query.all()
  """
  id = db.Column(db.Integer, primary_key=True)
  username = db.Column(db.String(30), index=True, unique=True, nullable=False)
  displayname = db.Column(db.String(30), index=True, unique=True, nullable=False)
  email = db.Column(db.String(150), index=True, unique=True, nullable=False)
  password_hash = db.Column(db.String(150), nullable=False)
  about_me = db.Column(db.String(200), nullable=True)
  member_since = db.Column(db.DateTime, default=datetime.utcnow)
  last_seen = db.Column(db.DateTime, default=datetime.utcnow)
  authority = db.Column(db.String(20), index=True, default=Authority.user.name)
  # one-to-many relationship (one-side)
  # Post objects can use author refer to the User object (e.g. p.author where p is a Post object)
  posts = db.relationship('Post', backref='author', lazy='dynamic')
  edit_history = db.relationship('EditInfo', backref='editor', lazy=True)

  def set_password(self, password):
    self.password_hash = generate_password_hash(password)

  def check_password(self, password):
    return check_password_hash(self.password_hash, password)

  # must use Authority enum as parameter
  def set_authority(self, new_authority):
    self.authority = new_authority.name

  # return an Authority enum object
  def get_authority(self):
    return Authority[self.authority]

  def is_superuser(self):
    self_authority = self.get_authority()
    return self_authority == Authority.admin or self_authority == Authority.webmaster

  def is_banned(self):
    self_authority = self.get_authority()
    return self_authority == Authority.banned

  def avatar(self, size):
    # convert email to the correct format
    digest = md5(self.email.lower().encode('utf-8')).hexdigest()
    #  "identicon" returns a geometric design that is different for every email
    return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(digest, size)

  def __repr__(self):
    return '<User({}): {}>'.format(self.id, self.username)

  def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

  @staticmethod
  def verify_reset_password_token(token):
      try:
          id = jwt.decode(token, app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
      except:
          return
      return User.query.get(id)


class Post(db.Model):
  """       Post
  id              INTEGER
  body            VARCHAR (300)
  timestamp       DATETIME
  user_id         INTEGER         (for author)
  thread_id       INTEGER         (for thread)
  author          -> User         one-to-many relationship (many-side)
  thread          -> Thread       one-to-many relationship (many-side)
  edit_info       -> EditInfo     one-to-one relationship
  closed          BOOLEAN

  Example: 
  >>> u = User.query.get(1)
  >>> p1 = Post(body='First post.', author=u)
  >>> p2 = Post(body='My second post.', author=u)
  >>> db.session.add(p1)
  >>> db.session.add(p2)
  >>> db.session.commit()
  >>> u.posts.all()
  """
  id = db.Column(db.Integer, primary_key=True)
  body = db.Column(db.String(300), nullable=False)
  timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
  # one-to-many relationship (many-side)
  user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
  # one-to-many relationship (many-side)
  thread_id = db.Column(db.Integer, db.ForeignKey('thread.id'), nullable=False)
  edit_info = db.relationship('EditInfo', backref='post', lazy=True, uselist=False)
  closed = db.Column(db.Boolean, default=False)

  def is_topic(self):
    return self == self.thread.topic_post()

  def __repr__(self):
    return '<Post({}): {}>'.format(self.id, self.body)


class Section(db.Model):
  """       Section
  id              INTEGER
  name            VARCHAR (30)
  slogan          VARCHAR (60)        (a short description)
  description     VARCHAR (200)
  threads           -> [Thread,...]   one-to-many relationship (one-side)
  closed          BOOLEAN
  """
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(30), nullable=False)
  slogan = db.Column(db.String(60), nullable=True)
  description = db.Column(db.String(200), nullable=True)
  threads = db.relationship('Thread', backref='section', lazy='dynamic')
  closed = db.Column(db.Boolean, default=False)

  def thread_count(self):
    return self.threads.count()

  def newest_thread(self):
    return self.threads.order_by(desc(Thread.id)).first()

  def __repr__(self):
    return '<Section({}): {}>'.format(self.id, self.name)


class Thread(db.Model):
  """       Thread
  id              INTEGER
  topic           VARCHAR (60)
  section         -> Sections         one-to-many relationship (many-side)
  posts           -> [Post,...]       one-to-many relationship (one-side)
  viewers         -> [User, ...]      many-to-many relationship
  closed          BOOLEAN
  pinned          BOOLEAN
  """
  id = db.Column(db.Integer, primary_key=True)
  topic = db.Column(db.String(60), nullable=False)
  section_id = db.Column(db.Integer, db.ForeignKey('section.id'), nullable=False)
  posts = db.relationship('Post', backref='thread', lazy='dynamic')
  viewers = db.relationship('User', secondary=viewers, lazy='subquery',
      backref=db.backref('viewed_threads', lazy=True))
  closed = db.Column(db.Boolean, default=False)
  pinned = db.Column(db.Boolean, default=False)

  def views(self):
    return len(self.viewers)

  def reply_count(self):
    return self.posts.count()-1

  def topic_post(self):
    return self.posts.first()

  def author(self):
    return self.topic_post().author

  def timestamp(self):
    return self.topic_post().timestamp

  def __repr__(self):
    return '<Thread({}): {}>'.format(self.id, self.topic)


class EditInfo(db.Model):
  """       EditInfo (for Post)
  id              INTEGER
  reason          VARCHAR (150)
  timestamp       DATETIME
  post_id         INTEGER             (for post)
  user_id         INTEGER             (for editor)
  post            -> Post             one-to-one relationship
  editor          -> User             one-to-many relationship (many-side)
  """
  id = db.Column(db.Integer, primary_key=True)
  reason = db.Column(db.String(150), nullable=True)
  timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
  post_id = db.Column(db.Integer, db.ForeignKey('post.id'), nullable=False)
  user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

  def __repr__(self):
    return '<EditInfo({}): {} by {}>'.format(self.id, self.reason, self.editor)