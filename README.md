# Financial Planning and Management Web Application
* CS 410-01: Intro to Software Engineering Projects Fall 2020
* Instructor: Kenneth K. Fletcher kenneth.fletcher@umb.edu

#### Admin
* Aimee Fiankou, UMass Boston, aimee.fiankou001@umb.edu

#### Description
* Financial literacy and planning the financial future of our families are topics
that everyone should be concerned with as the future is uncertain, and death is inevitable.
They should be viewed as critical matters that must be addressed in order to be prepared for
inevitable situations that will arise in the future and to avoid leaving our loved ones bearing
financial burdens. A comprehensive financial education and plan provides peace of mind,
protection and security for everyone involved. These topics are essential for all people but
even more so for parents of children with special needs and those who have loved ones that
may not be able to provide for themselves financially. Thus, parents should have a financial
plan in place to continue the care of the child or individual with special needs for when the
parent(s) or caregivers are no longer able to be providers.
In this project you will build a web application that will include but is not limited to the
following functions: a sign-up option, a navigation toolbar, videos and language options.

#### Skills and Technologies Required
* Python
* HTML
* CSS
* Javascript

#### Resources
* Flask
* Bootstrap
* Google Translate
* Google Search API

#### Team
* Adil Merribi adil.merribi001@umb.edu
* Jake Bates joshua.bates001@umb.edu
* Nik Costello nik.costello001@umb.edu
* Scott Reinhardt scott.reinhardt001@umb.edu
* Xu Huang Lin xuhuang.lin001@umb.edu

User Manual (Non-Technical "How-To" Guide):
https://docs.google.com/document/d/10FjabTduP6gXavbYvYKc1t_mU1WlL7zpkk6jXHpYizc/edit

User Technical Documentation Guide:
https://docs.google.com/document/d/1cXFqllVnjDEDf3mZGuHYFT-1T5j9Upn4TljObsX1hEQ/edit?usp=sharing

## Demo
* http://tway.pythonanywhere.com/
