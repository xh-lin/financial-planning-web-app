"""new pinned field in Thread table

Revision ID: 0bf1de6fcb4d
Revises: 66f74c9d4436
Create Date: 2020-11-26 20:02:36.651924

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0bf1de6fcb4d'
down_revision = '66f74c9d4436'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('thread', schema=None) as batch_op:
        batch_op.add_column(sa.Column('pinned', sa.Boolean(), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('thread', schema=None) as batch_op:
        batch_op.drop_column('pinned')

    # ### end Alembic commands ###
