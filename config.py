import os

# get the main directory of the application
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
        """For global variables.
        You can access these variables with a dictionary format.
        Example:
        from application import app
        ...
        app.config['SECRET_KEY']
        """
        SECRET_KEY = os.environ.get('SECRET_KEY') or "secret_string"
        # tell Flask-SQLAlchemy extension where to put the database file
        SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                'sqlite:///' + os.path.join(basedir, 'app.db')
        # Do not signal the application every time a change is about to be made in the database
        SQLALCHEMY_TRACK_MODIFICATIONS = False
        
        # email server details:
        # os.environ variables are in file .flaskenv
        MAIL_SERVER = os.environ.get('MAIL_SERVER')
        MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
        MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None # enable encrypted connections?
        MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
        MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
        ADMINS = ['admin@example.com']

        # max number of threads to display in a page
        MAX_THREADS = 15
        # max number of posts to display in a page
        MAX_POSTS = 15
        # number pf neighbor pages of current page
        NEIGHBOR_PAGES = 2